# This Dockerfile creates a static build image for CI

FROM openjdk:8-jdk

# install OS packages
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget apt-utils tar unzip lib32stdc++6 lib32z1 build-essential llvm-dev libclang-dev clang
# We use this for xxd hex->binary
RUN apt-get --quiet install --yes vim-common

# Install Rust-Lang
ENV HOME /root
ENV CARGO_HOME $HOME/.cargo
RUN mkdir $CARGO_HOME
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="${PATH}:${CARGO_HOME}/bin"
RUN rustup --version; \
    cargo --version; \
    rustc --version

# Install Rust Build Targets
RUN rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android
RUN rustup component add clippy

# Install bindgen
RUN cargo install bindgen
